# Tic Tac Tron 
A Tic Tac Toe Game Powered By Prolog AI

## Creator
- Muhammad Rafif Elfazri (1806205722)
- Giffari Faqih Phrasya Hardani (1806205634)

## Description

Tic Tac Tron is an intuitive 1 vs Comp Tic Tac Toe Game that use SWI-Prolog as the AI BackBone. Tic Tac Tron uses SWI-Prolog CLI as the User Interface. There are two game modes, **Normal** and **Impossible**. The Normal mode are in **Winnable_Tic_Tac_Tron.pl** file and The Impossible mode are In **Impossible_Tic_Tac_Tron.pl** file. We guarantee that **YOU CAN'T BEAT THE IMPOSSIBLE MODE.**

## Requirements
1. SWI-Prolog

## How To Use

There is 2 way to play this game:

### Playing It From Source Code
1. Run the source code with swipl
```bash
# For Normal Mode
$ swipl Winnable_Tic_Tac_Tron.pl

# For Impossible Mode
$ swipl Impossible_Tic_Tac_Tron.pl
```
2. Insert query 'start' on SWI-Prolog
```prolog
?- start.
```
4. Play The Game (***Notes :*** every user input must end with '.' character')

6. Insert query 'start' on SWI-Prolog to play again.

### Compile It
1. Compile the code with SWI-Prolog
	- For Linux User :
	```bash
	# For Normal mode
	$ swipl --stand_alone=true -o Winnable_Tic_Tac_Tron -g start -c Winnable_Tic_Tac_Tron.pl
	
	# For Impossible Mode
	$ swipl --stand_alone=true -o Impossible_Tic_Tac_Tron -g start -c Impossible_Tic_Tac_Tron.pl
	```
	- For Windows User :
		- Windows needs this .dll file to compile the source code:
			-   libswipl.dll
			-   libdwarf.dll
			-   libgmp-10.dll
			-   libwinpthread-1.dll
			-   libgcc_s_seh-1.dll
		- Compile It
		```powershell
		# For Normal mode
		$ swipl.exe -o Winnable_Tic_Tac_Tron.exe -c Winnable_Tic_Tac_Tron.pl --goal=start
		
		# For Impossible mode
		$ swipl.exe -o Impossible_Tic_Tac_Tron.exe -c Impossible_Tic_Tac_Tron.pl --goal=start 
		```
2.  Run it with your terminal
	```bash
	# For Linux
	$ ./Winnable_Tic_Tac_Tron  
	$ ./Impossible_Tic_Tac_Tron
	
	# For Windows
	$ Winnable_Tic_Tac_Tron.exe  
	$ Impossible_Tic_Tac_Tron.exe
	```
3. Play it Like The Source Code step except you don't need to insert any query.