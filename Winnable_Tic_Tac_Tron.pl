  /*
  Starter 
  */

  start :- user_start_input(X), play([' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '], X, 1).

  user_start_input(X) :- write('Enter 1 For First Turn, 2 For Second Turn:\n'), read(I), 
                        (validate_start_input(I) ->
                         X = I; nl, user_start_input(X)).
  validate_start_input(I) :- number(I), (I =:= 1, ! ; I =:= 2).



  /* 
  Display Tic Tac Toe Board With List Input
  */
  board(X, I) :- mod(I,3) =\= 2, nth0(I, X, Y), write(' '), write(Y), write(' |'), NextI is I+1, board(X, NextI), !.
  board(X, I) :- mod(I,3) =:= 2, I =\= 8, nth0(I, X, Y), write(' '), write(Y), write(' \n'), NextI is I+1, board(X, NextI), !.
  board(X, I) :-  I =:= 8, nth0(I, X, Y), write(' '), write(Y), write(' \n\n'), !.


  /* 
  Read User Input & Input Validation
  */
  user_input(X,Y,NX) :- user_input_help(A,B,X), count_index_tic_tac(A, B, Index), 
                        replace_index(X, Index, Y, NX), board(NX, 0).

  user_input_help(X,Y,ListTicTac) :- board(ListTicTac,0),
                                     write('Enter Row Number (1 <= X <= 3):\n'), read(I), 
                                     write('Enter Column Number (1 <= X <= 3):\n'), read(J),
                                     (validate_input_tic_tac(I, J, ListTicTac) ->
                                          (X = I, Y = J); nl, user_input_help(X,Y, ListTicTac)).

  validate_input_tic_tac(A, B, ListTicTac):- number(A), number(B),
                                             A > 0, A < 4, B > 0, B < 4, 
                                             count_index_tic_tac(A, B, Index),
                                             nth0(Index, ListTicTac, C), C = ' '.

  /* 
  Rule For replacing Index And Turn Tic Tac Toe Row
  Column Format to List Index
  */
  replace_index([_|T], 0, X, [X|T]).
  replace_index([H|T], I, X, [H|R]) :- I > -1, NI is I - 1, replace_index(T, NI, X, R), !.
  count_index_tic_tac(Row, Col, Index) :- Row < 4, Row > 0, Col < 4, Col > 0, 
                                          NRow is Row-1, NCol is Col-1, 
                                          Index is NRow*3 + NCol.
  /*
  Playing Logic
  */
  play(Board, 1, Move) :- Move < 10, \+ check_win(Board, o), 
                          user_input(Board, x, NBoard), 
                          NewMove is Move + 1, play(NBoard, 2, NewMove).

  play(Board, 1, Move) :- Move < 10, check_win(Board, o), board(Board, 0), result(2).

  play(Board, 2, Move) :- Move < 3, randomizer(Board, Pos), replace_index(Board, Pos, o, NBoard), 
                          NewMove is Move + 1, 
                          play(NBoard, 1, NewMove).

  play(Board, 2, Move) :- Move > 2, Move < 10, \+ check_win(Board, x), 
                          computer_logic(Board, NBoard), 
                          NewMove is Move + 1, 
                          play(NBoard, 1, NewMove).

  play(Board, 2, Move) :- Move < 10, check_win(Board, x), board(Board, 0), result(1).

  /*
  End Of Move
  */
  play(Board,_,10) :- check_win(Board, X), (X = x, board(Board, 0), result(1) ; X = o, board(Board, 0), result(2)), !.

  play(Board,_,10) :- \+check_win(Board,_), board(Board, 0), result(3).


  computer_logic(Board, NewBoard) :- findBestMove(Board,BestMove), replace_index(Board,BestMove,o,NewBoard).

  randomizer(List, Res) :- random(0, 9, X), ((nth0(X, List, Val), Val = ' ', Res is X); randomizer(List, Res)).

  /* 
  Check Winner
  */
  check_win([A,A,A,_,_,_,_,_,_], X) :- A = X, !.
  check_win([_,_,_,A,A,A,_,_,_], X) :- A = X, !. 
  check_win([_,_,_,_,_,_,A,A,A], X) :- A = X, !.
  check_win([A,_,_,A,_,_,A,_,_], X) :- A = X, !.
  check_win([_,A,_,_,A,_,_,A,_], X) :- A = X, !.
  check_win([_,_,A,_,_,A,_,_,A], X) :- A = X, !.
  check_win([A,_,_,_,A,_,_,_,A], X) :- A = X, !.
  check_win([_,_,A,_,A,_,A,_,_], X) :- A = X, !.


  /*
   * If check if there's whitespace in board (can move)
   */
  can_move(Board) :- member(' ', Board).

  /*
   * To check the score, based on the state of the board
   */
  % If computer has won, then the score is 10
  check_score(Board,Score):-check_win(Board,'o'),Score is 10.
  % If player 1 has won, then the score is -10
  check_score(Board,Score):-check_win(Board,'x'),Score is (-10).
  % If no one has won, then the score is 0
  check_score(Board,Score):- \+ check_win(Board,'o'),\+ check_win(Board,'x'), Score is 0.

  /*
   * For finding the minimax value
   */
  /*
   * If the score is 10, i.e., player 2 is winning, then result is the score
   */
  minimax(Board,Depth,_,Res):-check_score(Board,Score),Score=:=10,Res is Score-Depth,!.
  /*
   * If the score is -10, i.e., player 1 is winning, then result is the score
   */
  minimax(Board,Depth,_,Res):-check_score(Board,Score),Score=:=(-10),Res is Score+Depth,!.
  /*
   * If no moves are left, then result is 0
   */
  minimax(Board,_,_,Res):- \+can_move(Board),Res=0,!.

  minimax(Board,Depth,Last,Res):- can_move(Board),Last='x',Best is (-1000),
      NewDepth is Depth + 1,
      (
      ((nth0(0,Board,Value1),Value1=' ',replace_index(Board,0,'o',NBoard1),minimax(NBoard1,NewDepth,'o',Res1));Res1 is Best),
      ((nth0(1,Board,Value2),Value2=' ',replace_index(Board,1,'o',NBoard2),minimax(NBoard2,NewDepth,'o',Res2));Res2 is Best),
      ((nth0(2,Board,Value3),Value3=' ',replace_index(Board,2,'o',NBoard3),minimax(NBoard3,NewDepth,'o',Res3));Res3 is Best),
      ((nth0(3,Board,Value4),Value4=' ',replace_index(Board,3,'o',NBoard4),minimax(NBoard4,NewDepth,'o',Res4));Res4 is Best),
      ((nth0(4,Board,Value5),Value5=' ',replace_index(Board,4,'o',NBoard5),minimax(NBoard5,NewDepth,'o',Res5));Res5 is Best),
      ((nth0(5,Board,Value6),Value6=' ',replace_index(Board,5,'o',NBoard6),minimax(NBoard6,NewDepth,'o',Res6));Res6 is Best),
      ((nth0(6,Board,Value7),Value7=' ',replace_index(Board,6,'o',NBoard7),minimax(NBoard7,NewDepth,'o',Res7));Res7 is Best),
      ((nth0(7,Board,Value8),Value8=' ',replace_index(Board,7,'o',NBoard8),minimax(NBoard8,NewDepth,'o',Res8));Res8 is Best),
      ((nth0(8,Board,Value9),Value9=' ',replace_index(Board,8,'o',NBoard9),minimax(NBoard9,NewDepth,'o',Res9));Res9 is Best)
      ),
      max_list([Res1,Res2,Res3,Res4,Res5,Res6,Res7,Res8,Res9],Res).

  minimax(Board,Depth,Last,Res):- can_move(Board),Last='o',Best is 	1000,
      NewDepth is Depth + 1,
      (
      ((nth0(0,Board,Value1),Value1=' ',replace_index(Board,0,'x',NBoard1),minimax(NBoard1,NewDepth,'x',Res1));Res1 is Best),
      ((nth0(1,Board,Value2),Value2=' ',replace_index(Board,1,'x',NBoard2),minimax(NBoard2,NewDepth,'x',Res2));Res2 is Best),
      ((nth0(2,Board,Value3),Value3=' ',replace_index(Board,2,'x',NBoard3),minimax(NBoard3,NewDepth,'x',Res3));Res3 is Best),
      ((nth0(3,Board,Value4),Value4=' ',replace_index(Board,3,'x',NBoard4),minimax(NBoard4,NewDepth,'x',Res4));Res4 is Best),
      ((nth0(4,Board,Value5),Value5=' ',replace_index(Board,4,'x',NBoard5),minimax(NBoard5,NewDepth,'x',Res5));Res5 is Best),
      ((nth0(5,Board,Value6),Value6=' ',replace_index(Board,5,'x',NBoard6),minimax(NBoard6,NewDepth,'x',Res6));Res6 is Best),
      ((nth0(6,Board,Value7),Value7=' ',replace_index(Board,6,'x',NBoard7),minimax(NBoard7,NewDepth,'x',Res7));Res7 is Best),
      ((nth0(7,Board,Value8),Value8=' ',replace_index(Board,7,'x',NBoard8),minimax(NBoard8,NewDepth,'x',Res8));Res8 is Best),
      ((nth0(8,Board,Value9),Value9=' ',replace_index(Board,8,'x',NBoard9),minimax(NBoard9,NewDepth,'x',Res9));Res9 is Best)
      ),
      min_list([Res1,Res2,Res3,Res4,Res5,Res6,Res7,Res8,Res9],Res).

  findBestMove(Board,NPos):-Best is (-1000),
      (
      ((nth0(0,Board,Value1),Value1=' ',replace_index(Board,0,'o',NBoard1),minimax(NBoard1,0,'o',Res1));Res1 is Best),
      ((nth0(1,Board,Value2),Value2=' ',replace_index(Board,1,'o',NBoard2),minimax(NBoard2,0,'o',Res2));Res2 is Best),
      ((nth0(2,Board,Value3),Value3=' ',replace_index(Board,2,'o',NBoard3),minimax(NBoard3,0,'o',Res3));Res3 is Best),
      ((nth0(3,Board,Value4),Value4=' ',replace_index(Board,3,'o',NBoard4),minimax(NBoard4,0,'o',Res4));Res4 is Best),
      ((nth0(4,Board,Value5),Value5=' ',replace_index(Board,4,'o',NBoard5),minimax(NBoard5,0,'o',Res5));Res5 is Best),
      ((nth0(5,Board,Value6),Value6=' ',replace_index(Board,5,'o',NBoard6),minimax(NBoard6,0,'o',Res6));Res6 is Best),
      ((nth0(6,Board,Value7),Value7=' ',replace_index(Board,6,'o',NBoard7),minimax(NBoard7,0,'o',Res7));Res7 is Best),
      ((nth0(7,Board,Value8),Value8=' ',replace_index(Board,7,'o',NBoard8),minimax(NBoard8,0,'o',Res8));Res8 is Best),
      ((nth0(8,Board,Value9),Value9=' ',replace_index(Board,8,'o',NBoard9),minimax(NBoard9,0,'o',Res9));Res9 is Best)
      ),
      max_list([Res1,Res2,Res3,Res4,Res5,Res6,Res7,Res8,Res9],Res),
      nth0(NPos,[Res1,Res2,Res3,Res4,Res5,Res6,Res7,Res8,Res9],Res).

  /*
  Write Result to Terminal
  */
  result(1) :- write('You Win'), !.
  result(2) :- write('You Lose'), !.
  result(_) :- write('Draw, No Winner').
